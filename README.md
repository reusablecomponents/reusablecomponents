Add 'XMPPFramework' framework in your project

In ViewController class

Step 1: In your ViewController.swift file, import the XMPPFramework framework at the top.
write import XMPPFramework
Step 2: In the ViewDidLoad( ) method, Confuger the xmpp component (call setUpXMPP method)
Step 3: After configuration add the xmppDelegateModel.swift call in project.
Step 4: In the ViewController.swift file, add functionality for send button(Add message body and call send method for stream).
