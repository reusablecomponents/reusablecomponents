//
//  MessageDataClass.swift
//  ChatView
//
//  Created by osvinuser on 11/10/16.
//  Copyright © 2016 osvinuser. All rights reserved.
//

import Foundation
import UIKit

// 1. Type Enum

/**
 Enum specifing the type
 
 - SenderID  : Chat message is outgoing
 - ReciverID : Chat message is incoming
 
 */
enum messageSentType: Int {
    case Sent = 0
    case Received = 1
}


// Message Data model Class.
class MessageDataClass {

    // 2.Properties
    var text    : String?
    var msgDate : String?
    var image   : String?
    var date    : Date?
    var type    : messageSentType
    
    // 3. Initialization
    init(text: String?, msgDate: String?, date: Date? , type: messageSentType, image : String) {
        
        // Default type is Mine
        self.text = text
        self.msgDate = msgDate
        self.date = date
        self.type = type
        self.image = image
        
    }
    
}
