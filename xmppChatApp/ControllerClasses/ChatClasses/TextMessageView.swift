//
//  TextMessageView.swift
//  ChatView
//
//  Created by osvinuser on 11/10/16.
//  Copyright © 2016 osvinuser. All rights reserved.
//

import UIKit


class TextMessageView: UIView {
    
    
    var heightSuperView: CGFloat = 10.0
    
    var screenWidth = UIScreen.main.bounds.size.width
    
    init(messageData: MessageDataClass) {
        super.init(frame: TextMessageView.framePrimary(type: messageData.type))
        
        heightSuperView = self.frame.size.height
        
        let padding: CGFloat = 10.0
        
        let maxWidth = screenWidth - 100 /* This is a max width of message text */
        
        let widthAndHeightProfilePic: CGFloat = 40.0
        
        
        let backgroudView: UIView = UIView(frame: CGRect(x: messageData.type == .Received ? 90 : padding,
                                                         y: padding,
                                                         width: maxWidth,
                                                         height: heightSuperView))
        
        self.addSubview(backgroudView)
        
        
        //Set Profile Picture
        let senderProfilePic: UIImageView = self.setProfilePicOfSender(messageData                  :   messageData,
                                                                       xForImage                    :   messageData.type == .Received ? maxWidth - widthAndHeightProfilePic : 0,
                                                                      
                                                                       widthAndHeightProfilePic     :   widthAndHeightProfilePic)
        
        
        
        
        //Set Shadow to Image
        
        let shadowViewForProfilePic = UIView(frame : senderProfilePic.frame)
        shadowViewForProfilePic.layer.cornerRadius = senderProfilePic.layer.frame.size.height/2
        shadowViewForProfilePic.clipsToBounds = true
        
        addShadowToView(viewRefrence: shadowViewForProfilePic)
        
        backgroudView.addSubview(shadowViewForProfilePic)
        backgroudView.addSubview(senderProfilePic)

        
        
        //Colored Chatbox
        
        let chatBoxView: UIView = self.setTextInMessageLabel(messageData: messageData,
                                                             X: messageData.type == .Received ? 0 : senderProfilePic.frame.width+10,
                                                             Y: 0,
                                                             Width: backgroudView.frame.width-senderProfilePic.frame.width-10,
                                                             Height: heightSuperView)
    
        
        heightSuperView = chatBoxView.frame.size.height < 50 ? 50 : chatBoxView.frame.size.height
        
        chatBoxView.layer.cornerRadius = 4.0
        chatBoxView.layer.backgroundColor = messageData.type == .Received ? UIColor.blue.cgColor : UIColor.red.cgColor
        
        backgroudView.addSubview(chatBoxView)

        
        
        
        //Chonch
        
        let trianglePic = UIImageView(frame : messageData.type == .Received ?
            CGRect(x: senderProfilePic.frame.origin.x-10, y: 25, width: 10, height: 10)
            : CGRect(x: chatBoxView.frame.origin.x-9, y: 25, width: 10 , height: 10))
        
        
        trianglePic.image = messageData.type == .Received ? UIImage(named : "pointRight") : UIImage(named : "pointLeft")
        
        backgroudView.addSubview(trianglePic)
        
        
        
        
        //Time label
        
        let timeLabel : UIView = self.setTextInTimeLabel(messageData: messageData,
                                                         
                                                         frameOfTime: CGRect(x: messageData.type == .Received ? screenWidth - 100 - 60 : 60,
                                                                             
                                                                             y: chatBoxView.layer.frame.height+15,
                                                                             
                                                                             width: 100,
                                                                             
                                                                             height: 10 ))

        self.addSubview(timeLabel)
        

        self.frame.size.height = heightSuperView+25
    }
    
    
    // 2. Initialization coder
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    //MARK:- FRAME CALCULATION
    class func framePrimary(type: messageSentType) -> CGRect {
        return CGRect(x: 0, y: 1, width: UIScreen.main.bounds.size.width, height: 50)
    }
    
    
    
   
    func addShadowToView(viewRefrence : UIView)  {
        viewRefrence.layer.shadowColor = UIColor.lightGray.cgColor
        viewRefrence.layer.shadowRadius = 2;
        viewRefrence.layer.shadowOpacity = 0.5;
        viewRefrence.layer.shadowOffset = CGSize(width : -1, height : -1);
    }
    
    
    
    //MARK:- Profile Picture
    
    private func setProfilePicOfSender(messageData: MessageDataClass, xForImage: CGFloat, widthAndHeightProfilePic: CGFloat) -> UIImageView {
        
        let senderDp: UIImageView = UIImageView(frame: CGRect(x: xForImage, y: 0 , width: widthAndHeightProfilePic, height: widthAndHeightProfilePic))
        
        senderDp.image = UIImage(named : messageData.image!)
        senderDp.layer.cornerRadius = senderDp.layer.frame.size.height/2
        senderDp.clipsToBounds = true
        
        return senderDp
    }
    
    
    
    
    
    
    //MARK:- Make a Chat Box
  
    private func setTextInMessageLabel(messageData: MessageDataClass, X : CGFloat, Y : CGFloat, Width: CGFloat, Height : CGFloat) -> UIView {
        
        let messageTextLabel: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        
        let backView = UIView()
        
        // Set text in label.
        if let chatText = messageData.text {
            
            backView.frame = CGRect(x: X, y: Y, width: Width , height: Height)
            
            messageTextLabel.frame = CGRect(x: 10, y: 10, width: Width-20 , height: Height-20)
            
            messageTextLabel.textAlignment = messageData.type == .Received ? .right : .left
            messageTextLabel.font =  UIFont.systemFont(ofSize: 13)
            messageTextLabel.numberOfLines = 0
            messageTextLabel.textColor = messageData.type == .Received ? UIColor.darkGray : UIColor.white
            messageTextLabel.text = chatText
            messageTextLabel.sizeToFit()
            
            messageTextLabel.frame = CGRect(x: 10, y: 10, width: messageTextLabel.frame.size.width > Width-20 ? Width-20 : messageTextLabel.frame.size.width  , height: messageTextLabel.frame.size.height)
            
            messageTextLabel.text = chatText
            
            let heightToReturn =  messageTextLabel.frame.size.height+20
            
            backView.frame = CGRect(x: messageData.type == .Sent ? X : screenWidth - 100 - 50 - messageTextLabel.frame.size.width-20  , y: Y, width: messageTextLabel.frame.size.width+20 , height: heightToReturn)
            
            backView.addSubview(messageTextLabel)
        }
        
        return backView
    }
    

    
    
    private func setTextInTimeLabel(messageData: MessageDataClass, frameOfTime : CGRect) -> UIView {
        
        let timeLabel: UILabel = UILabel(frame: frameOfTime)
        
        if let chatTime = messageData.msgDate {
            
            timeLabel.frame = frameOfTime
            timeLabel.textAlignment = messageData.type == .Received ? .right : .left
            timeLabel.font = UIFont.systemFont(ofSize: 8)
            timeLabel.numberOfLines = 1
            timeLabel.textColor = UIColor.darkGray
            timeLabel.text = chatTime
            timeLabel.backgroundColor = UIColor.clear
        }
        
        return timeLabel
    }
}

