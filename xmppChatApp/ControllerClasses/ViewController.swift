//
//  ViewController.swift
//  xmppChatApp
//
//  Created by signity on 10/05/18.
//  Copyright © 2018 signity. All rights reserved.
//

import UIKit
import XMPPFramework

class ViewController: UIViewController {
    
    var stream:XMPPStream!
    var xmppRoster: XMPPRoster!
    let xmppRosterStorage = XMPPRosterCoreDataStorage()
    var messageArray = [MessageDataClass]()

    
    @IBOutlet var tableView: UITableView!
    @IBOutlet weak var btnSendMessage: UIButton!
    @IBOutlet weak var textViewMessage: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        self.setUpXMPP()
        self.loadMessages()
        // Do any additional setup after loading the view, typically from a nib.
    }
    func setUpXMPP() {
        xmppRoster = XMPPRoster(rosterStorage: xmppRosterStorage)
        stream = XMPPStream()
        xmppDelegateModel.shared.configureXMPPStreamRoster(stream, xmppRoster: xmppRoster)
        updateSubscribePresence = { presence in
            self.xmppRoster.subscribePresence(toUser: presence?.from())
        }
    }
    @IBAction func sendMessage(sender: UIButton) {
        let message = self.textViewMessage.text
        let senderJID = XMPPJID(string: "user2@localhost")
        let msg = XMPPMessage(type: "chat", to: senderJID)
        msg?.addBody(message)
        stream.send(msg)
    }
  
    
    func loadMessages() {
        for k in 0..<10 {
            
            let messageString = "Lorem ipsum dorem dskds dsijisdj sijd jiasdjsaidj sjdajsd sjdosajdo"
            // set isSent 1 if message is send and 0 if message is recived
            let isSent = k % 2 == 0 ? false : true
            
            let sent1: MessageDataClass = MessageDataClass(
                text: messageString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines),
                //Set message date
                msgDate: "Now",
                date: Date(),
                type: isSent == true ? .Sent : .Received,
                image: "userImage"// Set user image
            )
            self.messageArray.append(sent1)
            self.tableView.reloadData()
        }
    }

}

extension ViewController: UITableViewDataSource, UITableViewDelegate, UITextViewDelegate {
  
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messageArray.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : MessageTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cellIdentifier", for: indexPath) as! MessageTableViewCell
        cell.tag = indexPath.row
        cell.backgroundColor = UIColor.clear
        
        for view in cell.contentView.subviews {
            view.removeFromSuperview()
        }
        
        let messageDataObject:MessageDataClass = messageArray[indexPath.row]
        let textMessageView: TextMessageView = TextMessageView(messageData: messageDataObject)
        cell.contentView.addSubview(textMessageView)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let messageDataObject: MessageDataClass = messageArray[indexPath.row]
        let textMessageView: TextMessageView = TextMessageView(messageData: messageDataObject)
        return textMessageView.frame.size.height + 1
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
}
