//
//  xmppDelegateModel.swift
//  xmppChatApp
//
//  Created by signity on 10/05/18.
//  Copyright © 2018 signity. All rights reserved.
//

import UIKit
import XMPPFramework

var updateSubscribePresence: ((XMPPPresence?) -> Void)?
var updateAvailablePresence: ((XMPPPresence?) -> Void)?

class xmppDelegateModel: NSObject, XMPPStreamDelegate {
    static let shared = xmppDelegateModel()
    
    func configureXMPPStreamRoster(_ stream:XMPPStream, xmppRoster: XMPPRoster){
        stream.addDelegate(self, delegateQueue: DispatchQueue.main)
        xmppRoster.activate(stream)
        stream.myJID = XMPPJID(string: "user1@localhost")
        do {
            try stream.connect(withTimeout: 30)
        }
        catch {
            print("catch")
        }
    }
    
    public func xmppStreamWillConnect(_ sender: XMPPStream!) {
        print("will connect")
    }
    public func xmppStreamConnectDidTimeout(_ sender: XMPPStream!) {
        print("timeout:")
    }
    
    public func xmppStreamDidConnect(_ sender: XMPPStream!) {
        print("connected")
        do {
            try sender.authenticate(withPassword: "123")
        }
        catch {
            print("catch")
        }
    }
    public func xmppStreamDidAuthenticate(_ sender: XMPPStream!) {
        print("auth done")
        sender.send(XMPPPresence())
    }
    public func xmppStream(_ sender: XMPPStream!, didNotAuthenticate error: DDXMLElement!) {
        print("dint not auth")
        print(error)
    }
    public func xmppStream(_ sender: XMPPStream!, didReceive presence: XMPPPresence!) {
        print(presence)
        let presenceType = presence.type()
        let username = sender.myJID.user
        let presenceFromUser = presence.from().user
        
        if presenceFromUser != username  {
            if presenceType == "available" {
                updateAvailablePresence!(presence)
                print("available")
            }
            else if presenceType == "subscribe" {
                updateSubscribePresence!(presence)
            }
            else {
                print("presence type")
                print(presenceType ?? "No Presence Type Defined")
            }
        }
    }
    public func xmppStream(_ sender: XMPPStream!, didSend message: XMPPMessage!) {
        print("sent")
    }
    public func xmppStream(_ sender: XMPPStream!, didFailToSend iq: XMPPIQ!, error: Error!) {
        print("error")
    }
    public func xmppStream(_ sender: XMPPStream!, didReceiveError error: DDXMLElement!) {
        print("error")
    }
    public func xmppStream(_ sender: XMPPStream!, didFailToSend message: XMPPMessage!, error: Error!) {
        print("fail")
    }
    public func xmppStream(_ sender: XMPPStream!, didReceive message: XMPPMessage!) {
        print(message)
    }
}
